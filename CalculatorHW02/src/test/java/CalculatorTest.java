import cz.cvut.fel.ts1.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    static Calculator calc;

    @BeforeAll
    public static void initCalc() {
        calc = new Calculator();
    }


    @Test
    public void add_sumOf2and5_7() {
        int expected = 7;

        int res = calc.add(2, 5);
        Assertions.assertEquals(expected, res);
    }

    @Test
    public void subtract_subOf2and2_0() {
        int expected = 0;

        int res = calc.subtract(2, 2);
        Assertions.assertEquals(expected, res);
    }

    @Test
    public void multiply_6times5_30() {
        int expected = 30;

        int res = calc.multiply(6, 5);
        Assertions.assertEquals(expected, res);
    }

    @Test
    public void divide_60by20_3() {
        int expected = 3;

        int res = calc.divide(60, 20);
        Assertions.assertEquals(expected, res);
    }

    @Test
    public void divide_60by0_err() {
        Assertions.assertThrows(ArithmeticException.class, () -> {
            calc.divide(60, 0);
        });
    }
}
